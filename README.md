# at86rf215-driver: OS-independent driver for the AT86RF215 transceiver
This project provides an OS-independent implementation for
the for the AT86RF215 transceiver.


## Usage
The integration of the driver is quite easy and the effort required is quite minimal.
Users should provide the implementation of the driver functions declaraed as `weak`.
For convenience those are listed below:

```c
int
at86rf215_set_rstn(struct at86rf215 *h, uint8_t enable);

int
at86rf215_set_seln(struct at86rf215 *h, uint8_t enable);

void
at86rf215_delay_us(struct at86rf215 *h, uint32_t us);

size_t
at86rf215_get_time_ms(struct at86rf215 *h);

int
at86rf215_spi_read(struct at86rf215 *h, uint8_t *out, const uint8_t *in,
                   size_t tx_len, size_t rx_len);

int
at86rf215_spi_write(struct at86rf215 *h, const uint8_t *in, size_t len);

int
at86rf215_irq_user_callback(struct at86rf215 *h, uint8_t rf09_irqs,
                            uint8_t rf24_irqs, uint8_t bbc0_irqs,
                            uint8_t bbc1_irqs);

```

Depending on the system, more than one instances of the driver maybe required.
For this reason, the device handle can hold function pointers that the users may find useful.

```c
struct at86rf215 {
  ...
  void  *spi_dev;
  void  *cs_gpio_dev;
  void  *rst_gpio_dev;
  void  *clk_dev;
  void  *user_dev0;
  void  *user_dev1;
  void  *user_dev2;
  void  *user_dev3;
}
```

The last step required is to register the AT86RF215 IRQ callback on the IRQ handler of the target system.

```c
int
at86rf215_irq_callback(struct at86rf215 *h);
```

The driver should be able to operate without any modification of its source code.
If any custom action is needed after an IRQ event, users may implement it inside the `at86rf215_irq_user_callback()` which is called internally by the `at86rf215_irq_callback()`.

Below is a list of projects that utilize the driver:
* [SatNOGS-COMMS](https://gitlab.com/librespacefoundation/satnogs-comms/satnogs-comms-software-mcu)
* [SIDLOC](https://gitlab.com/librespacefoundation/sidloc/sidloc-mcu-sw)


## Development Guide
The development is performed on the master branch.
For special cases where a team of developers should work an a common feature,
maintainers may add a special branch on the repository.
However, it will be removed at the time it will be merged on the master branch.
All developers should derive the master branch for their feature branches and merge
requests should also issued at this branch.

Before submitting a new merge request, rebase the master branch and
confirm that the automated CI tests have successfully completed.

### Coding Style
At the root directory of the project there is the `clang-format` options
file `.clang-format` containing the proper configuration.
Developers can import this configuration to their favorite editor.
Failing to comply with the coding style described by the `.clang-format`
will result to failure of the automated tests running on our CI services.
So make sure that you import on your editor the coding style rules.

In addition the `hooks/pre-commit` file contains a Git hook,
that can be used to perform before every commit, code style formatting
with `clang-format` and the `.clang-format` parameters.
To enable this hook developers should copy the hook at their `.git/hooks`
directory.
Failing to comply with the coding style described by the `.clang-format`
will result to failure of the automated tests running on our CI services.
So make sure that you either import on your editor the coding style rules
or use the `pre-commit` Git hook.

## Website and Contact
For more information about the project and Libre Space Foundation please visit our [site](https://libre.space/)
and our [community forums](https://community.libre.space).

## License

![Libre Space Foundation](docs/assets/LSF_HD_Horizontal_Color1-300x66.png)
&copy; 2016-2019 [Libre Space Foundation](https://libre.space).

Licensed under the [GPLv3](LICENSE).
